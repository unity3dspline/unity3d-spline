﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(MeshRenderer))]
public class SplineRoad : MonoBehaviour
{
    public bool drawSpline = true;
    public BezierCurve[] curves;

    public ExtrudeShape extrudeShape = new ExtrudeShape()
    {
        vertices = new[]
        {
            new ExtrudeShape.Vertex(Vector3.zero, Vector3.up, 0),
            new ExtrudeShape.Vertex(Vector3.right * 0.25f, Vector3.up, 0.25f),
            new ExtrudeShape.Vertex(Vector3.right * 0.5f, Vector3.up, 0.5f),
            new ExtrudeShape.Vertex(Vector3.right * 0.75f, Vector3.up, 0.75f),
            new ExtrudeShape.Vertex(Vector3.right * 1f, Vector3.up, 1f)
        }
    };


    GameObject splineObject;
    public GameObject SplineObject
    {
        get
        {
            if (splineObject == null)
            {
                Transform trn = transform.FindChild("SplineObject");
                if (trn == null || trn.gameObject == null)
                {
                    splineObject = new GameObject("SplineObject");
                    splineObject.transform.parent = this.transform;
                    splineObject.transform.localPosition = Vector3.zero;

                    UnityEditorInternal.ComponentUtility.CopyComponent(this.MeshRenderer);
                    UnityEditorInternal.ComponentUtility.PasteComponentAsNew(splineObject);

                    splineObject.AddComponent<MeshFilter>();
                }
                else {
                    splineObject = trn.gameObject;
                }

                CreateMesh();
            }

            return splineObject;
        }
    }

    MeshRenderer meshRenderer;
    MeshRenderer MeshRenderer
    {
        get
        {
            meshRenderer = meshRenderer ?? GetComponent<MeshRenderer>();
            return meshRenderer;
        }
    }

    public IEnumerable<BezierCurve> Curves
    {
        get
        {
            foreach (var curve in curves)
            {
                yield return curve;
            }
        }
    }

    public void ReconnectCurves(bool forceReconnect = false)
    {
        // Reconnect all of the curves
        if (curves.Length > 0 && (forceReconnect || curves[0].NextCurve == null))
        {
            BezierCurve prevCurve = curves[0];
            for (int i = 1; i < curves.Length; i++)
            {
                curves[i].PrevCurve = prevCurve;
                prevCurve = curves[i];
            }

            CreateMesh();
        }
    }

    public void Start()
    {
        ReconnectCurves();
    }

    public void AddCurve(Vector3 pt)
    {
        BezierCurve curve;
        if (curves.Length == 0)
        {
            curve = new BezierCurve(new[]
            {
                Vector3.zero,
                Vector3.forward * 5,
                Vector3.forward * 5 + Vector3.right * 5,
                Vector3.right * 5
            });
        }
        else
        {
            curve = new BezierCurve(curves.Last());
        }

        curves = curves.Concat(new[] { curve }).ToArray();

        if (Application.isEditor)
        {
            if (curves.Length - 1 == 0)
            {
                AddControlPoint(curves.Length - 1, 0, Color.cyan);
                AddControlPoint(curves.Length - 1, 1, Color.green);
            }

            AddControlPoint(curves.Length - 1, 2, Color.green);
            AddControlPoint(curves.Length - 1, 3, Color.cyan);
        }

        CreateMesh();
    }

    public void CreateMesh()
    {
        SplineObject.GetComponent<MeshFilter>().sharedMesh = null;

        BezierCurve curve = curves.FirstOrDefault();
        if (curve != null)
        {
            SplineObject.GetComponent<MeshFilter>().sharedMesh = curve.Extrude(extrudeShape, 1.0f);
        }
    }

    #region Editor Helpers
    void AddControlPoint(int curveIdx, int ptIdx, Color c)
    {
        GameObject go = new GameObject("Control Point");
        go.transform.parent = transform;
        go.transform.position = Curves.ElementAt(curveIdx).GetPoint(ptIdx) + transform.position;

        SplineControlPoint cp = go.AddComponent<SplineControlPoint>();
        cp.color = c;
        cp.curveIndex = curveIdx;
        cp.ptIndex = ptIdx;
    }

    public void ClearCurves()
    {
        transform.DestroyChildren();
        curves = new BezierCurve[] { };
    }

    void OnDrawGizmos()
    {
        ReconnectCurves();

        if (drawSpline)
        {
            foreach (BezierCurve c in Curves)
            {
                Gizmos.color = Color.gray;
                Gizmos.DrawLine(c.GetPoint((int)0) + transform.position, c.GetPoint((int)1) + transform.position);
                Gizmos.DrawLine(c.GetPoint((int)2) + transform.position, c.GetPoint((int)3) + transform.position);

                for (int i = 0; i < c.SampledPoints.Length - 1; i++)
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine(c.SampledPoints[i] + transform.position, c.SampledPoints[i + 1] + transform.position);
                }
            }
        }

    }
    #endregion Editor Helpers
}
