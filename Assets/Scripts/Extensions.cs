﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using System.Reflection;
using UnityEngine.Profiling;

public static class Extensions
{
    public static int DivRem(int dividend, int divisor, out int remainder)
    {
        int quotient = dividend / divisor;
        remainder = dividend - divisor * quotient;
        return quotient;
    }

    public static bool HasFlag(this System.Enum keys, System.Enum flag)
    {
        int keysVal = System.Convert.ToInt32(keys);
        int flagVal = System.Convert.ToInt32(flag);

        return (keysVal & flagVal) != 0;
    }
}

public static class RandomExtensions
{
    static public T Next<T>(this System.Random rand, ICollection<T> coll)
    {
        if (coll.Count == 0) return default(T);

        return coll.ElementAt(rand.Next() % coll.Count);
    }

    static public T Next<T>(this System.Random rand, IList<T> coll)
    {
        if (coll.Count == 0) return default(T);

        return coll[rand.Next() % coll.Count];
    }

    static public float Next(this System.Random rand, Vector2 range)
    {
        float min = range.x;
        float max = range.y;

        if (min == max) return min;

        return min + ((float)rand.NextDouble() * (max - min));
    }

    static public int NextInt(this System.Random rand, Vector2 range)
    {
        int min = (int)range.x;
        int max = (int)range.y;

        if (min == max) return min;

        return min + (rand.Next() % (max - min));
    }

    static public bool FlipCoin(this System.Random rand, int numTimes = 1)
    {
        return Enumerable.Range(0, numTimes).Any(o => (rand.Next() & 0x01) == 0x01);
    }
}

public static class LinqExtensions
{
    static public IEnumerable<T> Duplicate<T>(this IEnumerable<T> coll, int count, System.Func<T, int, T> pred)
    {
        foreach (var it in coll)
        {
            yield return it;
        }

        for (int i = 0; i < count; i++)
        {
            foreach (var it in coll)
            {
                yield return pred(it, i);
            }
        }
    }
}

public static class UnityExtensions
{
    /// <summary>
    /// Remove all child transforms from this GameObject
    /// </summary>
    /// <param name="gameObject"></param>
    public static void DestroyChildren(this GameObject gameObject)
    {
        DestroyChildren(gameObject.transform);
    }

    /// <summary>
    /// Remove all child transforms from this transform's GameObject
    /// </summary>
    public static void DestroyChildren(this Transform transform)
    {
        Enumerable.Range(0, transform.childCount)
            .Select(i => transform.GetChild(i))
            .ToList()
            .ForEach(t => UnityEngine.Object.DestroyImmediate(t.gameObject));
    }

    public static void Complete(this IEnumerator iter)
    {
        while (iter.MoveNext())
        {
            if (iter.Current is YieldInstruction)
            {
            }
            else if (iter.Current is IEnumerator)
            {
                (iter.Current as IEnumerator).Complete();
            }
        }
    }

    public static Mesh Combine(this IEnumerable<Mesh> meshList, Matrix4x4[] transforms)
    {
        Mesh result = new Mesh();

        Mesh[] meshes = meshList.ToArray();
        CombineInstance[] combine = new CombineInstance[meshes.Length];

        for (int i = 0; i < meshes.Length; i++)
        {
            combine[i].mesh = meshes[i];

            if (transforms != null)
            {
                combine[i].transform = transforms[i];
            }
        }

        result.CombineMeshes(combine, true, transforms != null);

        return result;
    }

    static IEnumerable<int> Range(int start, int end, int step)
    {
        for (int i = start; i < end; i += step)
        {
            yield return i;
        }
    }

    static IEnumerable<int> QuadIndices(this IEnumerable<int> indices)
    {
        foreach (var i in indices)
        {
            yield return (0 + i);
            yield return (3 + i);
            yield return (2 + i);
            yield return (0 + i);
            yield return (2 + i);
            yield return (1 + i);
        }
    }

    static Dictionary<int, List<int>> subMeshIndices = new Dictionary<int, List<int>>();
    public static Mesh ToMesh(this List<Vertex> verts)
    {
        foreach (var sm in subMeshIndices.Values)
        {
            sm.Clear();
        }

        Profiler.BeginSample("Extensions.ToMesh()");
        Mesh mesh = new Mesh();

        Profiler.BeginSample("Extensions.ToMesh(): Generating Indices");
        for (int i = 0; i < verts.Count; i += 4)
        {
            List<int> indices;
            if (subMeshIndices.TryGetValue(verts[i].subMesh, out indices) == false)
            {
                indices = new List<int>();
                subMeshIndices.Add(verts[i].subMesh, indices);
            }

            indices.Add(0 + i);
            indices.Add(3 + i);
            indices.Add(2 + i);
            indices.Add(0 + i);
            indices.Add(2 + i);
            indices.Add(1 + i);
        }
        Profiler.EndSample();


        Profiler.BeginSample("Extensions.ToMesh(): Adding vertices");
        mesh.vertices = verts.Select(v => v.position).ToArray();
        Profiler.EndSample();

        Profiler.BeginSample("Extensions.ToMesh(): Adding UVCoords");
        mesh.uv = verts.Select(v => v.uv).ToArray();
        Profiler.EndSample();

        Profiler.BeginSample("Extensions.ToMesh(): Adding Submeshes");
        mesh.subMeshCount = 4;
        foreach (var sm in subMeshIndices.Where(smi => smi.Value.Count > 0))
        {
            mesh.SetTriangles(sm.Value.ToArray(), sm.Key);
        }
        Profiler.EndSample();

        Profiler.BeginSample("Extensions.ToMesh(): Recalculating Constraints");
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        Profiler.EndSample();

        //UnityEditor.Unwrapping.GenerateSecondaryUVSet(mesh, new UnityEditor.UnwrapParam());

        Profiler.EndSample();
        return mesh;
    }
}

public class DeepEnumerator : IEnumerator
{
    Stack<IEnumerator> iterators = new Stack<IEnumerator>();

    public DeepEnumerator(IEnumerator iter)
    {
        iterators.Push(iter);
    }

    public object Current
    {
        get
        {
            return iterators.Peek().Current;
        }
    }

    public bool MoveNext()
    {
        bool res;

        while ((res = iterators.Peek().MoveNext()) == false && iterators.Count > 1)
        {
            iterators.Pop();
        }

        if (res == false)
        {
            res = iterators.Peek().MoveNext();
        }

        if (res && iterators.Peek().Current is IEnumerator)
        {
            iterators.Push(iterators.Peek().Current as IEnumerator);
        }

        return res;
    }

    public void Reset()
    {
        while (iterators.Count > 1)
        {
            iterators.Peek().Reset();
            iterators.Pop();
        }

        iterators.Peek().Reset();
    }
}

public static class FloatArrayExtensions
{
    public static float Sample(this float[] fArr, float t)
    {
        int count = fArr.Length;
        if (count == 0)
        {
            Debug.LogError("Unable to sample array - it has no elements.");
            return 0;
        }

        if (count == 1) return fArr[0];

        float f = t * (count - 1);
        int idLower = Mathf.FloorToInt(f);
        int idUpper = Mathf.FloorToInt(f + 1);

        if (idUpper >= count) return fArr[count - 1];
        if (idLower < 0) return fArr[0];

        return Mathf.Lerp(fArr[idLower], fArr[idUpper], f - idLower);
    }
}


public static class EditorHelper
{

    public const string PROP_SCRIPT = "m_Script";


    private static Texture2D s_WhiteTexture;
    public static Texture2D WhiteTexture
    {
        get
        {
            if (s_WhiteTexture == null)
            {
                s_WhiteTexture = new Texture2D(1, 1);
                s_WhiteTexture.SetPixel(0, 0, Color.white);
                s_WhiteTexture.Apply();
            }
            return s_WhiteTexture;
        }
    }
    private static GUIStyle s_WhiteTextureStyle;
    public static GUIStyle WhiteTextureStyle
    {
        get
        {
            if (s_WhiteTextureStyle == null)
            {
                s_WhiteTextureStyle = new GUIStyle();
                s_WhiteTextureStyle.normal.background = EditorHelper.WhiteTexture;
            }
            return s_WhiteTextureStyle;
        }
    }


    //static EditorHelper()
    //{
    //    SceneView.onSceneGUIDelegate -= OnSceneGUI;
    //    SceneView.onSceneGUIDelegate += OnSceneGUI;
    //}

    #region Methods

    public static bool AssertMultiObjectEditingNotSupportedHeight(SerializedProperty property, GUIContent label, out float height)
    {
        if (property.hasMultipleDifferentValues)
        {
            height = EditorGUIUtility.singleLineHeight;
            return true;
        }

        height = 0f;
        return false;
    }

    //public static bool AssertMultiObjectEditingNotSupported(Rect position, SerializedProperty property, GUIContent label)
    //{
    //    if (property.hasMultipleDifferentValues)
    //    {
    //        EditorGUI.LabelField(position, label, EditorHelper.TempContent("Multi-Object editing is not supported."));
    //        return true;
    //    }

    //    return false;
    //}

    #endregion

    #region SerializedProperty Helpers

    public static System.Type GetTargetType(this SerializedObject obj)
    {
        if (obj == null) return null;

        if (obj.isEditingMultipleObjects)
        {
            var c = obj.targetObjects[0];
            return c.GetType();
        }
        else
        {
            return obj.targetObject.GetType();
        }
    }

    /// <summary>
    /// Gets the object the property represents.
    /// </summary>
    /// <param name="prop"></param>
    /// <returns></returns>
    public static System.Type GetTargetObjectType(this SerializedProperty prop)
    {
        System.Type res = null;

        var path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        if (obj != null)
        {
            res = obj.GetType();

            var elements = path.Split('.');
            foreach (var element in elements)
            {
                if (element.Contains("["))
                {
                    var elementName = element.Substring(0, element.IndexOf("["));
                    var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                    res = GetFieldType_Imp(res, elementName);
                }
                else
                {
                    res = GetFieldType_Imp(res, element);
                }
            }
        }

        return res;
    }

    /// <summary>
    /// Gets the object the property represents.
    /// </summary>
    /// <param name="prop"></param>
    /// <returns></returns>
    public static object GetTargetObject(this SerializedProperty prop)
    {
        var path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        var elements = path.Split('.');
        foreach (var element in elements)
        {
            if (element.Contains("["))
            {
                var elementName = element.Substring(0, element.IndexOf("["));
                var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                obj = GetValue_Imp(obj, elementName, index);
            }
            else
            {
                obj = GetValue_Imp(obj, element);
            }
        }
        return obj;
    }

    //public static void SetTargetObjectOfProperty(SerializedProperty prop, object value)
    //{
    //    var path = prop.propertyPath.Replace(".Array.data[", "[");
    //    object obj = prop.serializedObject.targetObject;
    //    var elements = path.Split('.');
    //    foreach (var element in elements.Take(elements.Length - 1))
    //    {
    //        if (element.Contains("["))
    //        {
    //            var elementName = element.Substring(0, element.IndexOf("["));
    //            var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
    //            obj = GetValue_Imp(obj, elementName, index);
    //        }
    //        else
    //        {
    //            obj = GetValue_Imp(obj, element);
    //        }
    //    }

    //    if (Object.ReferenceEquals(obj, null)) return;

    //    try
    //    {
    //        var element = elements.Last();

    //        if (element.Contains("["))
    //        {
    //            //var tp = obj.GetType();
    //            //var elementName = element.Substring(0, element.IndexOf("["));
    //            //var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
    //            //var field = tp.GetField(elementName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
    //            //var arr = field.GetValue(obj) as System.Collections.IList;
    //            //arr[index] = value;
    //            var elementName = element.Substring(0, element.IndexOf("["));
    //            var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
    //            var arr = DynamicUtil.GetValue(element, elementName) as System.Collections.IList;
    //            if (arr != null) arr[index] = value;
    //        }
    //        else
    //        {
    //            //var tp = obj.GetType();
    //            //var field = tp.GetField(element, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
    //            //if (field != null)
    //            //{
    //            //    field.SetValue(obj, value);
    //            //}
    //            DynamicUtil.SetValue(obj, element, value);
    //        }

    //    }
    //    catch
    //    {
    //        return;
    //    }
    //}

    /// <summary>
    /// Gets the object that the property is a member of
    /// </summary>
    /// <param name="prop"></param>
    /// <returns></returns>
    public static object GetTargetObjectWithProperty(SerializedProperty prop)
    {
        var path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        var elements = path.Split('.');
        foreach (var element in elements.Take(elements.Length - 1))
        {
            if (element.Contains("["))
            {
                var elementName = element.Substring(0, element.IndexOf("["));
                var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                obj = GetValue_Imp(obj, elementName, index);
            }
            else
            {
                obj = GetValue_Imp(obj, element);
            }
        }
        return obj;
    }

    private static System.Type GetFieldType_Imp(System.Type sourceType, string name)
    {
        if (sourceType == null) return null;

        while (sourceType != null)
        {
            var f = sourceType.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (f != null)
                return f.FieldType;

            var p = sourceType.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p != null)
                return p.PropertyType;

            sourceType = sourceType.BaseType;
        }

        return null;
    }


    private static object GetValue_Imp(object source, string name)
    {
        if (source == null)
            return null;
        var type = source.GetType();

        while (type != null)
        {
            var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (f != null)
                return f.GetValue(source);

            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p != null)
                return p.GetValue(source, null);

            type = type.BaseType;
        }
        return null;
    }

    private static object GetValue_Imp(object source, string name, int index)
    {
        var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
        if (enumerable == null) return null;
        var enm = enumerable.GetEnumerator();
        //while (index-- >= 0)
        //    enm.MoveNext();
        //return enm.Current;

        for (int i = 0; i <= index; i++)
        {
            if (!enm.MoveNext()) return null;
        }
        return enm.Current;
    }


    //public static void SetEnumValue<T>(this SerializedProperty prop, T value) where T : struct
    //{
    //    if (prop == null) throw new System.ArgumentNullException("prop");
    //    if (prop.propertyType != SerializedPropertyType.Enum) throw new System.ArgumentException("SerializedProperty is not an enum type.", "prop");

    //    var tp = typeof(T);
    //    if (tp.IsEnum)
    //    {
    //        prop.enumValueIndex = prop.enumNames.IndexOf(System.Enum.GetName(tp, value));
    //    }
    //    else
    //    {
    //        int i = ConvertUtil.ToInt(value);
    //        if (i < 0 || i >= prop.enumNames.Length) i = 0;
    //        prop.enumValueIndex = i;
    //    }
    //}

    //public static void SetEnumValue(this SerializedProperty prop, System.Enum value)
    //{
    //    if (prop == null) throw new System.ArgumentNullException("prop");
    //    if (prop.propertyType != SerializedPropertyType.Enum) throw new System.ArgumentException("SerializedProperty is not an enum type.", "prop");

    //    if (value == null)
    //    {
    //        prop.enumValueIndex = 0;
    //        return;
    //    }

    //    int i = prop.enumNames.IndexOf(System.Enum.GetName(value.GetType(), value));
    //    if (i < 0) i = 0;
    //    prop.enumValueIndex = i;
    //}

    //public static void SetEnumValue(this SerializedProperty prop, object value)
    //{
    //    if (prop == null) throw new System.ArgumentNullException("prop");
    //    if (prop.propertyType != SerializedPropertyType.Enum) throw new System.ArgumentException("SerializedProperty is not an enum type.", "prop");

    //    if (value == null)
    //    {
    //        prop.enumValueIndex = 0;
    //        return;
    //    }

    //    var tp = value.GetType();
    //    if (tp.IsEnum)
    //    {
    //        int i = prop.enumNames.IndexOf(System.Enum.GetName(tp, value));
    //        if (i < 0) i = 0;
    //        prop.enumValueIndex = i;
    //    }
    //    else
    //    {
    //        int i = ConvertUtil.ToInt(value);
    //        if (i < 0 || i >= prop.enumNames.Length) i = 0;
    //        prop.enumValueIndex = i;
    //    }
    //}

    //public static T GetEnumValue<T>(this SerializedProperty prop) where T : struct, System.IConvertible
    //{
    //    if (prop == null) throw new System.ArgumentNullException("prop");

    //    try
    //    {
    //        var name = prop.enumNames[prop.enumValueIndex];
    //        return ConvertUtil.ToEnum<T>(name);
    //    }
    //    catch
    //    {
    //        return default(T);
    //    }
    //}

    public static System.Enum GetEnumValue(this SerializedProperty prop, System.Type tp)
    {
        if (prop == null) throw new System.ArgumentNullException("prop");
        if (tp == null) throw new System.ArgumentNullException("tp");
        if (!tp.IsEnum) throw new System.ArgumentException("Type must be an enumerated type.");

        try
        {
            var name = prop.enumNames[prop.enumValueIndex];
            return System.Enum.Parse(tp, name) as System.Enum;
        }
        catch
        {
            return System.Enum.GetValues(tp).Cast<System.Enum>().First();
        }
    }

    public static void SetPropertyValue(SerializedProperty prop, object value)
    {
        if (prop == null) throw new System.ArgumentNullException("prop");

        switch (prop.propertyType)
        {
            case SerializedPropertyType.Integer:
                prop.intValue = System.Convert.ToInt32(value);
                break;
            case SerializedPropertyType.Boolean:
                prop.boolValue = System.Convert.ToBoolean(value);
                break;
            case SerializedPropertyType.Float:
                prop.floatValue = System.Convert.ToSingle(value);
                break;
            case SerializedPropertyType.String:
                prop.stringValue = System.Convert.ToString(value);
                break;
            case SerializedPropertyType.Color:
                prop.colorValue = (Color)value;
                break;
            case SerializedPropertyType.ObjectReference:
                prop.objectReferenceValue = value as Object;
                break;
            case SerializedPropertyType.LayerMask:
                prop.intValue = (value is LayerMask) ? ((LayerMask)value).value : System.Convert.ToInt32(value);
                break;
            case SerializedPropertyType.Enum:
                //prop.enumValueIndex = ConvertUtil.ToInt(value);
                //prop.SetEnumValue(value);
                break;
            case SerializedPropertyType.Vector2:
                prop.vector2Value = (Vector2)value;
                break;
            case SerializedPropertyType.Vector3:
                prop.vector3Value = (Vector3)value;
                break;
            case SerializedPropertyType.Vector4:
                prop.vector4Value = (Vector4)value;
                break;
            case SerializedPropertyType.Rect:
                prop.rectValue = (Rect)value;
                break;
            case SerializedPropertyType.ArraySize:
                prop.arraySize = System.Convert.ToInt32(value);
                break;
            case SerializedPropertyType.Character:
                prop.intValue = System.Convert.ToInt32(value);
                break;
            case SerializedPropertyType.AnimationCurve:
                prop.animationCurveValue = value as AnimationCurve;
                break;
            case SerializedPropertyType.Bounds:
                prop.boundsValue = (Bounds)value;
                break;
            case SerializedPropertyType.Gradient:
                throw new System.InvalidOperationException("Can not handle Gradient types.");
        }
    }

    public static object GetPropertyValue(SerializedProperty prop)
    {
        if (prop == null) throw new System.ArgumentNullException("prop");

        switch (prop.propertyType)
        {
            case SerializedPropertyType.Integer:
                return prop.intValue;
            case SerializedPropertyType.Boolean:
                return prop.boolValue;
            case SerializedPropertyType.Float:
                return prop.floatValue;
            case SerializedPropertyType.String:
                return prop.stringValue;
            case SerializedPropertyType.Color:
                return prop.colorValue;
            case SerializedPropertyType.ObjectReference:
                return prop.objectReferenceValue;
            case SerializedPropertyType.LayerMask:
                return (LayerMask)prop.intValue;
            case SerializedPropertyType.Enum:
                return prop.enumValueIndex;
            case SerializedPropertyType.Vector2:
                return prop.vector2Value;
            case SerializedPropertyType.Vector3:
                return prop.vector3Value;
            case SerializedPropertyType.Vector4:
                return prop.vector4Value;
            case SerializedPropertyType.Rect:
                return prop.rectValue;
            case SerializedPropertyType.ArraySize:
                return prop.arraySize;
            case SerializedPropertyType.Character:
                return (char)prop.intValue;
            case SerializedPropertyType.AnimationCurve:
                return prop.animationCurveValue;
            case SerializedPropertyType.Bounds:
                return prop.boundsValue;
            case SerializedPropertyType.Gradient:
                throw new System.InvalidOperationException("Can not handle Gradient types.");
        }

        return null;
    }

    public static SerializedPropertyType GetPropertyType(System.Type tp)
    {
        if (tp == null) throw new System.ArgumentNullException("tp");

        if (tp.IsEnum) return SerializedPropertyType.Enum;

        var code = System.Type.GetTypeCode(tp);
        switch (code)
        {
            case System.TypeCode.SByte:
            case System.TypeCode.Byte:
            case System.TypeCode.Int16:
            case System.TypeCode.UInt16:
            case System.TypeCode.Int32:
                return SerializedPropertyType.Integer;
            case System.TypeCode.Boolean:
                return SerializedPropertyType.Boolean;
            case System.TypeCode.Single:
                return SerializedPropertyType.Float;
            case System.TypeCode.String:
                return SerializedPropertyType.String;
            case System.TypeCode.Char:
                return SerializedPropertyType.Character;
            default:
                {
                    //if (TypeUtil.IsType(tp, typeof(Color)))
                    //    return SerializedPropertyType.Color;
                    //else if (TypeUtil.IsType(tp, typeof(UnityEngine.Object)))
                    //    return SerializedPropertyType.ObjectReference;
                    //else if (TypeUtil.IsType(tp, typeof(LayerMask)))
                    //    return SerializedPropertyType.LayerMask;
                    //else if (TypeUtil.IsType(tp, typeof(Vector2)))
                    //    return SerializedPropertyType.Vector2;
                    //else if (TypeUtil.IsType(tp, typeof(Vector3)))
                    //    return SerializedPropertyType.Vector3;
                    //else if (TypeUtil.IsType(tp, typeof(Vector4)))
                    //    return SerializedPropertyType.Vector4;
                    //else if (TypeUtil.IsType(tp, typeof(Quaternion)))
                    //    return SerializedPropertyType.Quaternion;
                    //else if (TypeUtil.IsType(tp, typeof(Rect)))
                    //    return SerializedPropertyType.Rect;
                    //else if (TypeUtil.IsType(tp, typeof(AnimationCurve)))
                    //    return SerializedPropertyType.AnimationCurve;
                    //else if (TypeUtil.IsType(tp, typeof(Bounds)))
                    //    return SerializedPropertyType.Bounds;
                    //else if (TypeUtil.IsType(tp, typeof(Gradient)))
                    //    return SerializedPropertyType.Gradient;
                }
                return SerializedPropertyType.Generic;

        }
    }



    public static int GetChildPropertyCount(SerializedProperty property, bool includeGrandChildren = false)
    {
        var pstart = property.Copy();
        var pend = property.GetEndProperty();
        int cnt = 0;

        pstart.Next(true);
        while (!SerializedProperty.EqualContents(pstart, pend))
        {
            cnt++;
            pstart.Next(includeGrandChildren);
        }

        return cnt;
    }

    #endregion

    #region Path

    public static string EnsureNotEndsWith(this string str, string s)
    {
        if (str.EndsWith(s, System.StringComparison.InvariantCultureIgnoreCase) == true)
        {
            return str.Substring(0, str.Length - s.Length);
        }

        return str;
    }

    public static string EnsureNotStartWith(this string str, string s)
    {
        if (str.StartsWith(s, System.StringComparison.InvariantCultureIgnoreCase) == true)
        {
            return str.Substring(s.Length);
        }

        return str;
    }

    public static string GetFullPathForAssetPath(string assetPath)
    {
        return Application.dataPath.EnsureNotEndsWith("Assets") + "/" + assetPath.EnsureNotStartWith("/");
    }

    #endregion

    #region Temp Content

    //private static TrackablObjectCachePool<GUIContent> _temp_text = new TrackablObjectCachePool<GUIContent>(50);


    ///// <summary>
    ///// Single immediate use GUIContent for a label. Should be used immediately and not stored/referenced for later use.
    ///// </summary>
    ///// <param name="text"></param>
    ///// <returns></returns>
    //public static GUIContent TempContent(string text)
    //{
    //    var c = _temp_text.GetInstance();
    //    c.text = text;
    //    c.tooltip = null;
    //    return c;
    //}

    //public static GUIContent TempContent(string text, string tooltip)
    //{
    //    var c = _temp_text.GetInstance();
    //    c.text = text;
    //    c.tooltip = tooltip;
    //    return c;
    //}

    #endregion

    #region Indent Helper

    private static Stack<int> _indents = new Stack<int>();

    public static void SuppressIndentLevel()
    {
        _indents.Push(EditorGUI.indentLevel);
        EditorGUI.indentLevel = 0;
    }

    public static void SuppressIndentLevel(int tempLevel)
    {
        _indents.Push(EditorGUI.indentLevel);
        EditorGUI.indentLevel = tempLevel;
    }

    public static void ResumeIndentLevel()
    {
        if (_indents.Count > 0)
        {
            EditorGUI.indentLevel = _indents.Pop();
        }
    }

    #endregion




    #region Event Handlers

    //private static void OnSceneGUI(SceneView scene)
    //{
    //    foreach (var c in _temp_text.ActiveMembers.ToArray())
    //    {
    //        _temp_text.Release(c);
    //    }

    //    _indents.Clear();
    //}

    #endregion

}
