﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class BezierCurve
{
    [SerializeField]
    Vector3[] points = new Vector3[4];

    BezierCurve prevCurve;
    public BezierCurve PrevCurve
    {
        get { return prevCurve; }
        set
        {
            if (prevCurve != value)
            {
                this.sampledLengths = null;
                this.sampledPoints = null;
            }

            if (prevCurve != null)
            {
                prevCurve.nextCurve = null;
            }

            if (value != null)
            {
                points[0] = value.points[3];
                Vector3 dir = (value.points[3] - value.points[2]);
                points[1] = points[0] + dir;

                value.NextCurve = this;
            }

            prevCurve = value;
        }
    }

    BezierCurve nextCurve;
    public BezierCurve NextCurve
    {
        get { return nextCurve; }
        set
        {
            if (nextCurve != value)
            {
                this.sampledLengths = null;
                this.sampledPoints = null;
            }

            if (nextCurve != null)
            {
                nextCurve.PrevCurve = null;
            }

            if (value != null)
            {
                points[3] = value.points[0];
                Vector3 dir = (value.points[1] - value.points[0]);
                points[2] = points[3] - dir;

                value.prevCurve = this;
            }

            nextCurve = value;
        }
    }

    public BezierCurve(BezierCurve prev, BezierCurve next = null)
    {
        this.prevCurve = prev;
        this.nextCurve = next;

        this.points = new Vector3[4];
        this.PrevCurve = prev;
        Vector3 dir = (points[1] - prevCurve.points[0]).normalized;
        points[2] = points[1] + dir * 2;
        points[3] = points[1] + dir * 3;
        this.NextCurve = next;
    }

    public BezierCurve(Vector3[] points)
    {
        this.points = points;
    }

    public Vector3 GetPoint(int idx) { return points[idx]; }

    public void SetPoint(int idx, Vector3 val)
    {
        sampledLengths = null;
        sampledPoints = null;

        points[idx] = val;

        // Adjust any linked curves
        if (prevCurve != null)
        {
            if (idx == 0)
            {
                prevCurve.points[3] = points[0];
                prevCurve.sampledLengths = null;
                prevCurve.sampledPoints = null;
            }
            else if (idx == 1)
            {
                Vector3 dir = points[1] - points[0];
                prevCurve.points[2] = prevCurve.points[3] - dir;
                prevCurve.sampledLengths = null;
                prevCurve.sampledPoints = null;
            }
        }
        if (nextCurve != null)
        {
            if (idx == 3)
            {
                nextCurve.points[0] = points[3];
                nextCurve.sampledLengths = null;
                nextCurve.sampledPoints = null;
            }
            else if (idx == 2)
            {
                Vector3 dir = points[3] - points[2];
                nextCurve.points[1] = nextCurve.points[0] + dir;
                nextCurve.sampledLengths = null;
                nextCurve.sampledPoints = null;
            }
        }
    }

    float[] sampledLengths;
    public float[] SampledLengths
    {
        get
        {
            if (sampledLengths == null)
            {
                Vector3 prevPoint = points[0];
                Vector3 pt;
                float total = 0;

                sampledLengths = FloatRange(1.0f / 10.0f)
                    .Select(f =>
                    {
                        pt = GetPoint(f);
                        total += (pt - prevPoint).magnitude;
                        prevPoint = pt;

                        return total;
                    })
                    .ToArray();
            }

            return sampledLengths;
        }
    }

    /// <summary>
    /// Return the normal along a specific line
    /// </summary>
    /// <param name="t">Value between 0 and 1, indicating the time parameter</param>
    /// <param name="up">A normalized up vector</param>
    /// <returns></returns>
    Vector3 GetNormal(float t, Vector3 up)
    {
        return CalculateNormal(GetTangent(t), up);
    }

    Vector3 CalculateNormal(Vector3 tangent, Vector3 up)
    {
        Vector3 binormal = Vector3.Cross(up, tangent);
        return Vector3.Cross(tangent, binormal);
    }

    Vector3 CalculateTangent(float t, float t2, float it2)
    {
        return (points[0] * -it2 +
            points[1] * (t * (3 * t - 4) + 1) +
            points[2] * (-3 * t2 + t * 2) +
            points[3] * t2).normalized;
    }

    Vector3 CalculatePoint(float t, float t2, float t3, float it, float it2, float it3)
    {
        return points[0] * (it3) +
            points[1] * (3 * it2 * t) +
            points[2] * (3 * it * t2) +
            points[3] * t3;
    }

    public Vector3 GetTangent(float t)
    {
        float t2 = t * t;
        float it = (1 - t);
        float it2 = it * it;

        return CalculateTangent(t, t2, it2);
    }

    public Vector3 GetPoint(float t, out Vector3 tangent, out Vector3 normal, out Quaternion orientation)
    {
        float t2 = t * t;
        float t3 = t2 * t;
        float it = (1 - t);
        float it2 = it * it;
        float it3 = it * it * it;

        tangent = CalculateTangent(t, t2, it2);
        normal = CalculateNormal(tangent, Vector3.up);
        orientation = Quaternion.LookRotation(tangent, normal);

        return CalculatePoint(t, t2, t3, it, it2, it3);
    }

    public Vector3 GetPoint(float t, out Vector3 tangent, out Vector3 normal)
    {
        float t2 = t * t;
        float t3 = t2 * t;
        float it = (1 - t);
        float it2 = it * it;
        float it3 = it * it * it;

        tangent = CalculateTangent(t, t2, it2);
        normal = CalculateNormal(tangent, Vector3.up);

        return CalculatePoint(t, t2, t3, it, it2, it3);
    }

    public Vector3 GetPoint(float t, out Vector3 tangent)
    {
        float t2 = t * t;
        float t3 = t2 * t;
        float it = (1 - t);
        float it2 = it * it;
        float it3 = it * it * it;

        tangent = CalculateTangent(t, t2, it2);
        return CalculatePoint(t, t2, t3, it, it2, it3);
    }

    public Vector3 GetPoint(float t)
    {
        float t2 = t * t;
        float t3 = t2 * t;
        float it = (1 - t);
        float it2 = it * it;
        float it3 = it * it * it;

        return CalculatePoint(t, t2, t3, it, it2, it3);
    }

    public Quaternion GetOrientation(float t)
    {
        Vector3 tangent, normal;
        GetPoint(t, out tangent, out normal);

        return Quaternion.LookRotation(tangent, normal);
    }

    // A slightly faster method
    //public Vector3 GetPoint(float t)
    //{
    //    float t2 = t * t;
    //    float t3 = t2 * t;

    //    return points[0] * (-t3 + 3 * t2 - 3 * t + 1) +
    //        points[1] * (3 * t3 - 6 * t2 + 3 * t) +
    //        points[2] * (-3 * t3 + 3 * t2) +
    //        points[3] * t3;
    //}

    // This is the slowest method of lerping a spline
    //public Vector3 GetPoint(float t)
    //{
    //    Vector3 a = Vector3.Lerp(points[0], points[1], t);
    //    Vector3 b = Vector3.Lerp(points[1], points[2], t);
    //    Vector3 c = Vector3.Lerp(points[2], points[3], t);
    //    Vector3 d = Vector3.Lerp(a, b, t);
    //    Vector3 e = Vector3.Lerp(b, c, t);

    //    return Vector3.Lerp(d, e, t);
    //}

    public OrientedPoint GetOrientedPoint(float t)
    {
        Vector3 tangent, normal;
        Quaternion orientation;

        Vector3 point = GetPoint(t, out tangent, out normal, out orientation);

        return new OrientedPoint(point, orientation, SampledLengths.Sample(t));
    }

    List<int> meshLines = new List<int>();
    public Mesh ToMesh(Vector2[] verts, Vector2[] normals, float[] uCoords)
    {
        Mesh mesh = new Mesh();

        meshLines.Clear();
        for (int i = 0; i < verts.Length - 1; i++)
        {
            meshLines.Add(i);
            meshLines.Add(i + 1);
        }

        return mesh;
    }

    public IEnumerable<OrientedPoint> GeneratePath(float subDivisions)
    {
        float step = 1.0f / subDivisions;

        for (float f = 0; f < 1; f += step)
        {
            yield return GetOrientedPoint(f);
        }

        yield return GetOrientedPoint(1);
    }

    public void Extrude(Mesh mesh, ExtrudeShape shape, OrientedPoint[] path)
    {
        int vertsInShape = shape.vertices.Length;
        int segments = path.Length - 1;
        int edgeLoops = path.Length;
        int vertCount = vertsInShape * edgeLoops;
        int triCount = shape.Lines.Length * segments * 2;
        int triIndexCount = triCount * 3;

        int[] triangleIndices = new int[triIndexCount];
        Vector3[] vertices = new Vector3[vertCount];
        Vector3[] normals = new Vector3[vertCount];
        Vector2[] uvs = new Vector2[vertCount];

        // Generate all of the vertices and normals
        for (int i = 0; i < path.Length; i++)
        {
            int offset = i * vertsInShape;
            for (int j = 0; j < vertsInShape; j++)
            {
                int id = offset + j;
                vertices[id] = path[i].LocalToWorld(shape.vertices[j].vert);
                normals[id] = path[i].LocalToWorldDirection(shape.vertices[j].normal);
                uvs[id] = new Vector2(shape.vertices[j].uCoord, path[i].vCoordinate);
            }
        }

        // Generate all of the triangles
        int ti = 0;
        for (int i = 0; i < segments; i++)
        {
            int offset = i * vertsInShape;
            for (int l = 0; l < shape.Lines.Length; l += 2)
            {
                int a = offset + shape.Lines[l];
                int b = offset + shape.Lines[l] + vertsInShape;
                int c = offset + shape.Lines[l + 1] + vertsInShape;
                int d = offset + shape.Lines[l + 1];
                triangleIndices[ti++] = a;
                triangleIndices[ti++] = b;
                triangleIndices[ti++] = c;
                triangleIndices[ti++] = c;
                triangleIndices[ti++] = d;
                triangleIndices[ti++] = a;
            }
        }

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.normals = normals;
        mesh.uv = uvs;
        mesh.triangles = triangleIndices;
    }

    public Mesh Extrude(ExtrudeShape shape, float distancePerSegment)
    {
        List<Mesh> meshes = new List<Mesh>();
        BezierCurve curve = this;
        while (curve != null)
        {
            Mesh mesh = new Mesh();
            meshes.Add(mesh);

            OrientedPoint[] path = curve.GeneratePath(curve.TotalLength / distancePerSegment).ToArray();

            int vertsInShape = shape.vertices.Length;
            int segments = path.Length - 1;
            int edgeLoops = path.Length;
            int vertCount = vertsInShape * edgeLoops;
            int triCount = shape.Lines.Length * segments * 2;
            int triIndexCount = triCount * 3;

            int[] triangleIndices = new int[triIndexCount];
            Vector3[] vertices = new Vector3[vertCount];
            Vector3[] normals = new Vector3[vertCount];
            Vector2[] uvs = new Vector2[vertCount];

            // Generate all of the vertices and normals
            for (int i = 0; i < path.Length; i++)
            {
                int offset = i * vertsInShape;
                for (int j = 0; j < vertsInShape; j++)
                {
                    int id = offset + j;
                    vertices[id] = path[i].LocalToWorld(shape.vertices[j].vert);
                    normals[id] = path[i].LocalToWorldDirection(shape.vertices[j].normal);
                    uvs[id] = new Vector2(shape.vertices[j].uCoord, path[i].vCoordinate);
                }
            }

            // Generate all of the triangles
            int ti = 0;
            for (int i = 0; i < segments; i++)
            {
                int offset = i * vertsInShape;
                for (int l = 0; l < shape.Lines.Length; l += 2)
                {
                    int a = offset + shape.Lines[l];
                    int b = offset + shape.Lines[l] + vertsInShape;
                    int c = offset + shape.Lines[l + 1] + vertsInShape;
                    int d = offset + shape.Lines[l + 1];
                    triangleIndices[ti++] = a;
                    triangleIndices[ti++] = b;
                    triangleIndices[ti++] = c;
                    triangleIndices[ti++] = c;
                    triangleIndices[ti++] = d;
                    triangleIndices[ti++] = a;
                }
            }

            mesh.Clear();
            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uvs;
            mesh.triangles = triangleIndices;

            curve = curve.NextCurve;
        }

        return meshes.Combine(null);
    }

    public class CurveIntersection
    {
        public BezierCurve a, b;
        public float at, bt;
        public float arad, brad;
    }

    public float TotalLength
    {
        get
        {
            return SampledLengths.Last();
        }
    }

    Vector3[] sampledPoints;
    public Vector3[] SampledPoints
    {
        get
        {
            if (sampledPoints == null)
            {
                sampledPoints = FloatRange(1.0f / 10.0f)
                    .Select(f => SampledLengths.Sample(f) / TotalLength)
                    .Select(f => GetPoint(f))
                    .ToArray();
            }

            return sampledPoints;
        }
    }

    IEnumerable<float> FloatRange(float step)
    {
        for (float f = 0; f < 1.0f; f += step)
        {
            yield return f;
        }
        yield return 1;
    }

    Bounds boundingBox;
    public Bounds BoundingBox
    {
        get
        {
            if (boundingBox == null)
            {
                Vector3 center = (points[0] + points[1] + points[2] + points[3]) * 0.25f;
                Vector3 size = Vector3.zero;

                Vector3[] lines =
                {
                    points[1] - points[0],
                    points[2] - points[1],
                    points[3] - points[2],
                    points[0] - points[3]
                };

                foreach (Vector3 line in lines)
                {
                    size.x = Mathf.Max(size.x, Mathf.Abs(line.x));
                    size.y = Mathf.Max(size.y, Mathf.Abs(line.y));
                    size.z = Mathf.Max(size.z, Mathf.Abs(line.z));
                }

                boundingBox = new Bounds(center, size * 0.5f);
            }

            return boundingBox;
        }
    }

    public void FindClosestPoints(BezierCurve a, BezierCurve b, out float atMin, out float btMin)
    {
        float thresholdSq = 25;
        float[] floatRange = FloatRange(1.0f / 10.0f).ToArray();
        float aSqLen = a.TotalLength * a.TotalLength;
        float bSqLen = b.TotalLength * b.TotalLength;

        atMin = -1;
        btMin = -1;
        float minLen = float.MaxValue;

        // No point in even trying if these segments don't intersect
        if (a.BoundingBox.Intersects(b.BoundingBox) == false)
            return;

        // Find the two closest sampled points
        for (int ai = 0; ai < a.SampledPoints.Length; ai++)
        {
            for (int bi = 0; bi < b.SampledPoints.Length; bi++)
            {
                float len = (a.SampledPoints[ai] - b.SampledPoints[bi]).sqrMagnitude;
                if (len < minLen)
                {
                    minLen = len;
                    atMin = floatRange[ai];
                    btMin = floatRange[bi];

                    // The first time we hit our threshold, we're done!
                    if (minLen <= thresholdSq) return;
                }
            }
        }

        // If we haven't met the threshold, reset these to -1
        atMin = -1;
        btMin = -1;
    }
}

public struct Vertex
{
    public int subMesh;
    public Vector3 position;
    public Vector3 normal;
    public Vector2 uv;
}

[Serializable]
public class ExtrudeShape
{
    [Serializable]
    public class Vertex
    {
        public Vector2 vert;
        public Vector2 normal;
        public float uCoord;

        public Vertex(Vector2 vert, Vector2 normal, float uCoord)
        {
            this.vert = vert;
            this.normal = normal;
            this.uCoord = uCoord;
        }
    }

    public Vertex[] vertices;

    IEnumerable<int> LineSegment(int i)
    {
        yield return i;
        yield return i + 1;
    }

    int[] lines;
    public int[] Lines
    {
        get
        {
            if (lines == null)
            {
                lines = Enumerable.Range(0, vertices.Length - 1)
                    .SelectMany(i => LineSegment(i))
                    .ToArray();
            }

            return lines;
        }
    }
};

public struct OrientedPoint
{
    public Vector3 position;
    public Quaternion rotation;
    public float vCoordinate;

    public OrientedPoint(Vector3 position, Quaternion rotation, float vCoordinate = 0)
    {
        this.position = position;
        this.rotation = rotation;
        this.vCoordinate = vCoordinate;
    }

    public Vector3 LocalToWorld(Vector3 point)
    {
        return position + rotation * point;
    }

    public Vector3 WorldToLocal(Vector3 point)
    {
        return Quaternion.Inverse(rotation) * (point - position);
    }

    public Vector3 LocalToWorldDirection(Vector3 dir)
    {
        return rotation * dir;
    }
}
